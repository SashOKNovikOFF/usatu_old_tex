reset

set terminal jpeg
set output "3rdTask.jpg"
#set terminal postscript eps
#set output "3rdTask.eps"

set multiplot
set grid
unset key

set xrange [-10:10]
set yrange [-10:10]
set zrange [-200:200]
set xlabel "Axis X"
set ylabel "Axis Y"
set zlabel "Axis Z"

f(x, y) = (4*y*x**2 - 15*y)/(4*x**2 - 15)**(0.5)
splot f(x, y); splot -f(x, y)