reset

set terminal jpeg
set output "1stTask.jpg"
#set terminal postscript eps
#set output "1stTask.eps"

set multiplot
set grid
unset key

set xrange [-2:2]
set yrange [-5:5]
set xlabel "Axis X"
set ylabel "Axis Y"

const = -5

f(x) = (2**x**3/3 - 3*x**2 + const)**(0.5)

plot f(x); plot -f(x)
const = -4
plot f(x); plot -f(x)
const = -3
plot f(x); plot -f(x)
const = -2
plot f(x); plot -f(x)
const = -1
plot f(x); plot -f(x)
const = 0
plot f(x); plot -f(x)
const = 1
plot f(x); plot -f(x)
const = 2
plot f(x); plot -f(x)
const = 3
plot f(x); plot -f(x)
const = 4
plot f(x); plot -f(x)
const = 5
plot f(x); plot -f(x)