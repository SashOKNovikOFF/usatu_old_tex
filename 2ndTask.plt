reset

set terminal jpeg
set output "2ndTask.jpg"
#set terminal postscript eps
#set output "2ndTask.eps"

set multiplot
set grid
unset key

set xrange [-2:2]
set yrange [-5:5]
set zrange [-2:2]
set xlabel "Axis X"
set ylabel "Axis Y"
set zlabel "Axis Z"

f(x, y) = (1 + 2*(x*y)**(0.5)/3 - 8*x*y/9 + x**2)**(0.5)
splot f(x, y); splot -f(x, y)